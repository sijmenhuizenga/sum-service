package nl.han.ica.asd17b.smartmobility.sumservice;

import com.google.gson.Gson;
import static spark.Spark.*;

public class SumService {
    
    public static void main( String[] args ) {
        Gson gson = new Gson();
        
        port(80);
        
        get("/sum/:num1/:num2", (req, res) -> {
            try{
                int a = Integer.parseInt(req.params(":num1"));
                int b = Integer.parseInt(req.params(":num2"));
                return gson.toJson(new SumResult(a + b));
            }catch (Exception e){
                e.printStackTrace();
                return e.getMessage();
            }
        });
    }
}

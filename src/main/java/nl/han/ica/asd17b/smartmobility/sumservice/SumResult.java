package nl.han.ica.asd17b.smartmobility.sumservice;

/**
 * Created by Sijmen on 11-4-2017.
 */
public class SumResult {
    
    public int result;

    public SumResult(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}

FROM maven:3-jdk-8
MAINTAINER Sijmen

RUN mkdir -p /usr/src/app
RUN mkdir -p /usr/app
WORKDIR /usr/src/app

ADD . /usr/src/app

RUN mvn package
RUN cp /urs/src/app/som-service-1.1-jar-with-dependencies.jar /usr/app/service.jar

EXPOSE 80

CMD ["java", "-jar", "/usr/app/service.jar"]